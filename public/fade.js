$(document).ready(function() {
  $('.fade').css('opacity','0');
    /* Every time the window is scrolled ... */
    $(window).scroll( function(){

        /* Check the location of each desired element */
        $('.fade').each( function(i){

            let objectBottom = $(this).offset().top + (1/5)*$(this).outerHeight();
            let windowBottom = $(window).scrollTop() + $(window).height();

            /* If the object is completely visible in the window, fade it it */
          if (windowBottom > objectBottom) {
             //object comes into view (scrolling down)

        $(this).fadeTo("slow",1);

      } ;


        });

    });

});
